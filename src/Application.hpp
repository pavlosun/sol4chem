/*
 * Sol4Chem - a set of tools for chemists
 * Copyright (C) 2022 Pavlo Solntsev <p.sun.fun@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <gtkmm.h>

namespace ps {
class Application : public Gtk::Application {

    Glib::RefPtr<Gtk::CssProvider> m_refCssProvider;

  protected:
    Application();

  public:
    static auto create() -> Glib::RefPtr<Application>;
    void on_activate() override;
    void on_startup() override;

    void on_hide_window(Gtk::Widget *window);
    void on_action_quit();
    void on_css_parsing_error(const Glib::RefPtr<const Gtk::CssSection> &section, const Glib::Error &error);
};
} // namespace ps
