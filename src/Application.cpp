/*
 * Sol4Chem - a set of tools for chemists
 * Copyright (C) 2022 Pavlo Solntsev <p.sun.fun@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Application.hpp"
#include "config.hpp"

namespace ps {

Application::Application() : Gtk::Application(SOL4CHEM_ID) {
    Glib::set_application_name(SOL4CHEM_NAME);

    m_refCssProvider = Gtk::CssProvider::create();
}

auto Application::create() -> Glib::RefPtr<Application> {
    return Glib::RefPtr<Application>(new Application());
}

void Application::on_activate() {

    Glib::RefPtr<Gdk::Screen> screen = Gdk::Screen::get_default();

    Gtk::StyleContext::add_provider_for_screen(screen, m_refCssProvider,
                                               GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
    m_refCssProvider->signal_parsing_error().connect(
        sigc::mem_fun(*this, &Application::on_css_parsing_error));
    m_refCssProvider->load_from_resource("/com/sol4chem/styles/styles.css");
}

void Application::on_startup() {

    Gtk::Application::on_startup();

    add_action("quit", sigc::mem_fun(*this, &Application::on_action_quit));
    set_accel_for_action("app.quit", "<Ctrl>Q");
}

void Application::on_hide_window(Gtk::Widget *window) { delete window; }

void Application::on_action_quit() {
    auto windows = get_windows();

    for (const auto &i : windows)
        i->hide();
}

void Application::on_css_parsing_error([[maybe_unused]] const Glib::RefPtr<const Gtk::CssSection> &section,
                                       [[maybe_unused]] const Glib::Error &error) {}

} // namespace ps
